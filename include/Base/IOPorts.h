#ifndef IO_PORTS_H
#define IO_PORTS_H

#include <Types.h>

#define BRAND_QEMU 1
#define BRAND_VBOX 2

/**
 * read a byte from given port number
 */
uint8 inportb(uint16 port);

/**
 * write a given byte to given port number
 */
void outportb(uint16 port, uint8 val);

/**
 * read 2 bytes(short) from given port number
 */
uint16 inports(uint16 port);

/**
 * write given 2(short) bytes to given port number
 */
void outports(uint16 port, uint16 data);

/**
 * read 4 bytes(long) from given port number
 */
uint32 inportl(uint16 port);

/**
 * write given 4 bytes(long) to given port number
 */
void outportl(uint16 port, uint32 data);

/**
 * Internal function used in cpuid_info(int)
 */
void __cpuid(uint32 type, uint32* eax, uint32* ebx, uint32* ecx, uint32* edx);

/**
 * Show CPUInfo
 * @arg print Whether to print the CPUID output or not
 */
int cpuid_info(int print);

/**
 * Shuts down the system using emulator-specific methods
*/
void shutdown();

#endif
