#include <System/Shell.h>

#include <Base/IOPorts.h>
#include <Base/Multiboot.h>
#include <Device/IDE.h>
#include <Device/Keyboard.h>
#include <Graphics/Bitmap.h>
#include <Graphics/VESA.h>
#include <Kernel/GDT.h>
#include <Kernel/IDT.h>
#include <Kernel/KernelHeap.h>
#include <Kernel/PMM.h>
#include <String.h>
#include <System/Console.h>
#include <stdarg.h>
#include "Device/Mouse.h"

#define MATCH(a) strcmp(argv[0], a) == 0
#define MAX_ITEMS 128

struct BasicFileSystem {
    int zyncid;
    char* magic;
};

void printf_color(uint8 color, const char* format, va_list args) {
    console_set_color(color, COLOR_BLACK);
    printf(format, args);
    console_set_color(COLOR_GREY, COLOR_BLACK);
}

int is_echo(char* b) {
    if ((b[0] == 'e') && (b[1] == 'c') && (b[2] == 'h') && (b[3] == 'o'))
        if (b[4] == ' ' || b[4] == '\0')
            return TRUE;
    return FALSE;
}

void shell_main() {
    console_clear(COLOR_GREY, COLOR_BLACK);

    printf("Welcome to SystemZync kernel shell!\n\n");
    printf("-> Please beware that this shell provides REALLY low level access to your \nhardware and the SystemZync system.\n");
    printf("-> Please use this tool responsibly :^)\n\n");
    
    char buffer[255];
    const char* shell = "shell@zync> ";

    int argc = 0;
    char* argv[MAX_ITEMS];

    while (1) {
        // Prompt
        printf_color(COLOR_YELLOW, shell, 0);

        // Initialize buffer
        memset(buffer, 0, sizeof(buffer));

        // Reads the keyboard input into buffer
        read_string_within_bounds(buffer, strlen(shell));

        // Continue if empty string
        if (strlen(buffer) == 0)
            continue;

        // Tokenize
        argc = 0;
        argv[argc] = strtok(buffer, " ");

        while (argv[argc]) {
            argc++;
            argv[argc] = strtok(0, " ");
        }

        if (argc <= 0) {
            continue;
        }

        // Match all the commands
        if (MATCH("help")) {
            printf("SystemZync Kernel Shell (experimental)\n");
            printf("Commands: help, cpuid, echo, shutdown\n");
        } else if (MATCH("cpuid")) {
            cpuid_info(1);
        } else if (MATCH("shutdown")) {
            shutdown();
        } else if (MATCH("exit")) {
            printf("Exiting the shell will launch the GUI server. Continue?\n");

            kb_get_keycode();
            if (kb_get_keycode() == SCAN_CODE_KEY_ENTER) {
                break;
            }
        } else if (MATCH("vesacfg list")) {
            vbe_print_available_modes();
        } else if (MATCH("echo")) {
            for (int i = 1; i < argc; i++)
            {
                if (argv[i] != 0)
                    printf("%s ", argv[i]);
            }
            printf("\n");
        } else if (MATCH("write")) {
            ata_init();

            const int DRIVE = ata_get_drive_by_model("QEMU HARDDISK");
            const uint32 LBA = 0;
            const uint8 NO_OF_SECTORS = 1;
            char buf[ATA_SECTOR_SIZE] = {0};

            struct BasicFileSystem fs;

            fs.zyncid = 10012;
            fs.magic = "ZyncMagik";

            memset(buf, 0, sizeof(buf));
            memcpy(buf, &fs, sizeof(fs));

            ide_write_sectors(DRIVE, NO_OF_SECTORS, LBA, (uint32)buf);

            printf("%d bytes written to 'QEMU HARDDISK'\n", sizeof(buf));

        } else if (MATCH("read")) {
            const int DRIVE = ata_get_drive_by_model("QEMU HARDDISK");
            const uint32 LBA = 0;
            const uint8 NO_OF_SECTORS = 1;
            char buf[ATA_SECTOR_SIZE] = {0};

            memset(buf, 0, sizeof(buf));
            ide_read_sectors(DRIVE, NO_OF_SECTORS, LBA, (uint32)buf);
            struct BasicFileSystem fs;
            memcpy(&fs, buf, sizeof(fs));
            printf("Reading data from 'QEMU HARDDISK': %s, %d\n", fs.magic, fs.zyncid);

        } else {
            // Print error if no matching commands
            printf_color(COLOR_BRIGHT_RED, "invalid command: %s\n", buffer);
        }
    }
}