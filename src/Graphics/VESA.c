#include <Graphics/VESA.h>
#include <Kernel/BIOS32Handler.h>
#include <System/Console.h>
#include <Base/IOPorts.h>
#include <Kernel/ISR.h>
#include <String.h>
#include <Types.h>
#include <Device/Keyboard.h>
#include <Kernel/PMM.h>
#include <Kernel/KernelHeap.h>

// vbe information
VBE20_INFOBLOCK g_vbe_infoblock;
VBE20_MODEINFOBLOCK g_vbe_modeinfoblock;
// selected mode
int g_selected_mode = -1;
// selected mode width & height
uint32 g_width = 0, g_height = 0;
// buffer pointer pointing to video memory
uint32* g_vbe_buffer = NULL;
uint32* g_vbe_back_buffer = NULL;

uint32 vbe_get_width()
{
    return g_width;
}

uint32 vbe_get_height()
{
    return g_height;
}

// get vbe info
int get_vbe_info()
{
    REGISTERS16 in = {0}, out = {0};
    // set specific value 0x4F00 in ax to get vbe info into bios memory area
    in.ax = 0x4F00;
    // set address pointer to BIOS_CONVENTIONAL_MEMORY where vbe info struct will be stored
    in.di = BIOS_CONVENTIONAL_MEMORY;
    int86(0x10, &in, &out); // call video interrupt 0x10
    // copy vbe info data to our global variable g_vbe_infoblock
    memcpy(&g_vbe_infoblock, (void *)BIOS_CONVENTIONAL_MEMORY, sizeof(VBE20_INFOBLOCK));
    return (out.ax == 0x4F);
}

void get_vbe_mode_info(uint16 mode, VBE20_MODEINFOBLOCK *mode_info)
{
    REGISTERS16 in = {0}, out = {0};
    // set specific value 0x4F00 in ax to get vbe mode info into some other bios memory area
    in.ax = 0x4F01;
    in.cx = mode;                            // set mode info to get
    in.di = BIOS_CONVENTIONAL_MEMORY + 1024; // address pointer, different than used in get_vbe_info()
    int86(0x10, &in, &out);                  // call video interrupt 0x10
    // copy vbe mode info data to parameter mode_info
    memcpy(mode_info, (void *)BIOS_CONVENTIONAL_MEMORY + 1024, sizeof(VBE20_MODEINFOBLOCK));
}

void vbe_set_mode(uint32 mode)
{
    REGISTERS16 in = {0}, out = {0};
    // set any given mode, mode is to find by resolution X & Y
    in.ax = 0x4F02;
    in.bx = mode;
    int86(0x10, &in, &out); // call video interrupt 0x10 to set mode
}

// find the vbe mode by width & height & bits per pixel
uint32 vbe_find_mode(uint32 width, uint32 height, uint32 bpp)
{
    // iterate through video modes list
    uint16 *mode_list = (uint16 *)g_vbe_infoblock.VideoModePtr;
    uint16 mode = *mode_list++;
    while (mode != 0xffff)
    {
        // get each mode info
        get_vbe_mode_info(mode, &g_vbe_modeinfoblock);
        if (g_vbe_modeinfoblock.XResolution == width && g_vbe_modeinfoblock.YResolution == height && g_vbe_modeinfoblock.BitsPerPixel == bpp)
        {
            return mode;
        }
        mode = *mode_list++;
    }
    return -1;
}

/**
 * Copies the backbuffer to video memory
*/
void vbe_swap_buffer() {
    int buffer_size = g_vbe_modeinfoblock.XResolution * g_vbe_modeinfoblock.YResolution * (g_vbe_modeinfoblock.BitsPerPixel / 8);

    // Copy back buffer to video memory
    memcpy(&g_vbe_modeinfoblock.PhysBasePtr, g_vbe_back_buffer, buffer_size);
}

// print availabel modes to console
void vbe_print_available_modes()
{
    bios32_init();
    printf("Trying to initialize VESA VBE 2.0\n");
    
    if (!get_vbe_info()) {
        printf("No VESA VBE 2.0 detected!\n");
        return;
    }

    VBE20_MODEINFOBLOCK mode_info;

    // iterate through video modes list
    uint16 *mode_list = (uint16 *)g_vbe_infoblock.VideoModePtr;
    uint16 mode = *mode_list++;
    while (mode != 0xffff)
    {
        get_vbe_mode_info(mode, &mode_info);
        printf("[Component/VESA] Mode: %d, X: %d, Y: %d\n", mode, mode_info.XResolution, mode_info.YResolution);
        mode = *mode_list++;
    }

    kb_getchar();
}

// set rgb values in 32 bit number
uint32 from_rgb(uint8 red, uint8 green, uint8 blue)
{
    uint32 color = red;
    color <<= 16;
    color |= (green << 8);
    color |= blue;
    return color;
}

// put the pixel on the given x,y point
void vbe_putpixel(int x, int y, int color)
{
    // if (x < 0 || x > g_vbe_modeinfoblock.XResolution || y < 0 || y > g_vbe_modeinfoblock.YResolution)
    //     return;

    // if (x)
    //     x = (x * (g_vbe_modeinfoblock.BitsPerPixel >> 3));
    // if (y)
    //     y = (y * g_vbe_modeinfoblock.BytesPerScanLine);

    uint32 i = y * g_width + x;
    *(g_vbe_back_buffer + i) = color;

    // register char* cTemp;
    // cTemp = &g_vbe_buffer[x + y];
    // cTemp[0] = color & 0xff;
    // cTemp[1] = (color >> 8) & 0xff;
    // cTemp[2] = (color >> 16) & 0xff;
}

void vbe_clear_color(int color)
{
    // for (int y = 0; y < (int)vbe_get_height(); y++)
    //     for (int x = 0; x < (int)vbe_get_width(); x++)
    //         vbe_putpixel(x, y, color);
    int buffer_size = g_vbe_modeinfoblock.XResolution * g_vbe_modeinfoblock.YResolution * (g_vbe_modeinfoblock.BitsPerPixel / 8);

    memset(g_vbe_back_buffer, 0, buffer_size);
}

void vbe_draw_rect(int x, int y, int width, int height, int color)
{
    for (int ly = y; ly < height; ly++)
        for (int lx = x; lx < width; lx++)
            vbe_putpixel(lx, ly, color);
}

int vesa_init(uint32 width, uint32 height, uint32 bpp)
{
    bios32_init();
    printf("Initializing VESA VBE 2.0\n");
    if (!get_vbe_info())
    {
        printf("No VESA VBE 2.0 detected!\n");
        return -1;
    }
// set this to 1 to print all available modes to console
#define PRINT_MODES 0
#if PRINT_MODES
    printf("Press UP and DOWN arrow keys to scroll\n");
    printf("Modes:\n");
    vbe_print_available_modes();
    return 1;
#else
    g_selected_mode = vbe_find_mode(width, height, bpp);
    if (g_selected_mode == -1)
    {
        printf("Failed to find mode for %d-%d\n", width, height);
        return -1;
    }
    printf("\nSelected VESA mode: %d\n", g_selected_mode);
    // set selection resolution to width & height
    g_width = g_vbe_modeinfoblock.XResolution;
    g_height = g_vbe_modeinfoblock.YResolution;
    // set selected mode video physical address point to buffer for pixel plotting
    g_vbe_buffer = (uint32 *)g_vbe_modeinfoblock.PhysBasePtr;
    // set the mode to start graphics window
    vbe_set_mode(g_selected_mode);
#endif

    int buffer_size = g_vbe_modeinfoblock.XResolution * g_vbe_modeinfoblock.YResolution * (g_vbe_modeinfoblock.BitsPerPixel / 8);
    g_vbe_back_buffer = kmalloc(buffer_size);

    return 0;
}