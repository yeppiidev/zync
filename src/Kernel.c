#include <Kernel.h>


#include <Base/IOPorts.h>
#include <Base/Multiboot.h>
#include <Device/IDE.h>
#include <Device/Keyboard.h>
#include <Graphics/Bitmap.h>
#include <Graphics/VESA.h>
#include <Kernel/GDT.h>
#include <Kernel/IDT.h>
#include <Kernel/KernelHeap.h>
#include <Kernel/PMM.h>
#include <String.h>
#include <System/Console.h>
#include <System/Shell.h>
#include "Device/Mouse.h"

KERNEL_MEMORY_MAP g_kmap;

// Cursor bitmap
char* cursor = {
    "0000000000000000 "
    "0001000000000000 "
    "0001100000000000 "
    "0001110000000000 "
    "0001111000000000 "
    "0001111100000000 "
    "0001111110000000 "
    "0001111111000000 "
    "0001111111100000 "
    "0001111111110000 "
    "0000001111000000 "
    "0000000111100000 "
    "0000000111100000 "
    "0000000011110000 "
    "0000000000000000 "
    "0000000000000000 "
    "0000000000000000 ",
};

int get_kernel_memory_map(KERNEL_MEMORY_MAP* kmap, MULTIBOOT_INFO* mboot_info) {
    uint32 i;

    if (kmap == NULL)
        return -1;
    kmap->kernel.k_start_addr = (uint32)&__kernel_section_start;
    kmap->kernel.k_end_addr = (uint32)&__kernel_section_end;
    kmap->kernel.k_len = ((uint32)&__kernel_section_end - (uint32)&__kernel_section_start);

    kmap->kernel.text_start_addr = (uint32)&__kernel_text_section_start;
    kmap->kernel.text_end_addr = (uint32)&__kernel_text_section_end;
    kmap->kernel.text_len = ((uint32)&__kernel_text_section_end - (uint32)&__kernel_text_section_start);

    kmap->kernel.data_start_addr = (uint32)&__kernel_data_section_start;
    kmap->kernel.data_end_addr = (uint32)&__kernel_data_section_end;
    kmap->kernel.data_len = ((uint32)&__kernel_data_section_end - (uint32)&__kernel_data_section_start);

    kmap->kernel.rodata_start_addr = (uint32)&__kernel_rodata_section_start;
    kmap->kernel.rodata_end_addr = (uint32)&__kernel_rodata_section_end;
    kmap->kernel.rodata_len = ((uint32)&__kernel_rodata_section_end - (uint32)&__kernel_rodata_section_start);

    kmap->kernel.bss_start_addr = (uint32)&__kernel_bss_section_start;
    kmap->kernel.bss_end_addr = (uint32)&__kernel_bss_section_end;
    kmap->kernel.bss_len = ((uint32)&__kernel_bss_section_end - (uint32)&__kernel_bss_section_start);

    kmap->system.total_memory = mboot_info->mem_low + mboot_info->mem_high;

    for (i = 0; i < mboot_info->mmap_length; i += sizeof(MULTIBOOT_MEMORY_MAP)) {
        MULTIBOOT_MEMORY_MAP* mmap = (MULTIBOOT_MEMORY_MAP*)(mboot_info->mmap_addr + i);
        if (mmap->type != MULTIBOOT_MEMORY_AVAILABLE)
            continue;
        // make sure kernel is loaded at 0x100000 by bootloader(see linker.ld)
        if (mmap->addr_low == kmap->kernel.text_start_addr) {
            // set available memory starting from end of our kernel, leaving 1MB size for functions exceution
            kmap->available.start_addr = kmap->kernel.k_end_addr + 1024 * 1024;
            kmap->available.end_addr = mmap->addr_low + mmap->len_low;
            // get availabel memory in bytes
            kmap->available.size = kmap->available.end_addr - kmap->available.start_addr;
            return 0;
        }
    }

    return -1;
}

void display_kernel_memory_map(KERNEL_MEMORY_MAP* kmap) {
    printf("k_mmap:\n");
    printf("  kernel-start: 0x%x, kernel-end: 0x%x, TOTAL: %d bytes\n",
           kmap->kernel.k_start_addr, kmap->kernel.k_end_addr, kmap->kernel.k_len);
    printf("  text-start: 0x%x, text-end: 0x%x, TOTAL: %d bytes\n",
           kmap->kernel.text_start_addr, kmap->kernel.text_end_addr, kmap->kernel.text_len);
    printf("  data-start: 0x%x, data-end: 0x%x, TOTAL: %d bytes\n",
           kmap->kernel.data_start_addr, kmap->kernel.data_end_addr, kmap->kernel.data_len);
    printf("  rodata-start: 0x%x, rodata-end: 0x%x, TOTAL: %d\n",
           kmap->kernel.rodata_start_addr, kmap->kernel.rodata_end_addr, kmap->kernel.rodata_len);
    printf("  bss-start: 0x%x, bss-end: 0x%x, TOTAL: %d\n",
           kmap->kernel.bss_start_addr, kmap->kernel.bss_end_addr, kmap->kernel.bss_len);

    printf("total_memory: %d KB\n", kmap->system.total_memory);
    printf("available:\n");
    printf("  start_adddr: 0x%x\n  end_addr: 0x%x\n  size: %d\n",
           kmap->available.start_addr, kmap->available.end_addr, kmap->available.size);
}

void vesa_draw_boot_screen() {
    vbe_clear_color(FROM_RGB(0, 0, 0));

    bitmap_draw_string_upper("SystemZync", 30, 30, FROM_RGB(255, 255, 0));
    bitmap_draw_string("Press 1 for white and 2 for black...", 30, 60, FROM_RGB(0, 255, 255));

    int my = 0;
    int mx = 0;

    for (int i = 0; i < 272; i++) {
        if (cursor[i] == '1') {
            // Cursor
            vbe_putpixel(mouse_getx() + mx, mouse_gety() + my, FROM_RGB(255, 255, 0));

            // Shadow
            vbe_putpixel(mouse_getx() + mx + 1, mouse_gety() + my + 1, FROM_RGB(10, 10, 10));
            mx++;
        }
        if (cursor[i] == '0') {
            // Empty pixels
            mx++;
            continue;
        }
        if (cursor[i] == ' ') {
            my++;
            mx = 0;
        }
    }

    // Wait for vsync
    while ((inportb(0x3DA) & 0x08))
        ;
    while (!(inportb(0x3DA) & 0x08))
        ;

    vbe_swap_buffer();
}

void system_entry(unsigned long magic, unsigned long addr) {
    MULTIBOOT_INFO* mboot_info;

    gdt_init();
    idt_init();

    console_init(COLOR_GREY, COLOR_BLACK);

    keyboard_init();
    mouse_init();
    ata_init();

    if (magic == MULTIBOOT_BOOTLOADER_MAGIC) {
        mboot_info = (MULTIBOOT_INFO*)addr;
        memset(&g_kmap, 0, sizeof(KERNEL_MEMORY_MAP));
        if (get_kernel_memory_map(&g_kmap, mboot_info) < 0) {
            printf("error: failed to get kernel memory map\n");
            return;
        }

        // put the memory bitmap at the start of the available memory
        pmm_init(g_kmap.available.start_addr, g_kmap.available.size);
        // initialize atleast 1MB blocks of memory for our heap
        pmm_init_region(g_kmap.available.start_addr, PMM_BLOCK_SIZE * 256);
        // initialize heap 256 blocks(1MB)
        void* start = pmm_alloc_blocks(256);
        void* end = start + (pmm_next_free_frame(1) * PMM_BLOCK_SIZE);
        kheap_init(start, end);

        // shell_main();

        printf("Trying to initialize VESA...\n");

        int vesa_success = vesa_init(640, 480, 32);
        if (vesa_success < 0) {
            printf("Failed to initialize VESA graphics!\n");
            goto done;
        }

        if (vesa_success == 1) {
            // scroll to top
            for (int i = 0; i < MAXIMUM_PAGES; i++)
                console_scroll(SCROLL_UP);

            while (1) {
                // add scrolling to view all modes
                char c = kb_get_keycode();
                if (c == SCAN_CODE_KEY_UP)
                    console_scroll(SCROLL_UP);
                if (c == SCAN_CODE_KEY_DOWN)
                    console_scroll(SCROLL_DOWN);
            }
        } else {
            for (;;)
                vesa_draw_boot_screen();
        }

    done:
        printf("Unloading VESA driver...");

        pmm_free_blocks(start, 256);
        pmm_deinit_region(g_kmap.available.start_addr, PMM_BLOCK_SIZE * 256);
    } else {
        printf("error: invalid multiboot magic number\n");
    }
}
