#include <String.h>
#include <Base/IOPorts.h>
#include <System/Console.h>

/**
 * read a byte from given port number
 */
uint8 inportb(uint16 port) {
    uint8 ret;
    asm volatile("inb %1, %0"
                 : "=a"(ret)
                 : "Nd"(port));
    return ret;
}

/**
 * write a given byte to given port number
 */
void outportb(uint16 port, uint8 val) {
    asm volatile("outb %1, %0" ::"dN"(port), "a"(val));
}

/**
 * read 2 bytes(short) from given port number
 */
uint16 inports(uint16 port) {
    uint16 rv;
    asm volatile("inw %1, %0"
                 : "=a"(rv)
                 : "dN"(port));
    return rv;
}

/**
 * write given 2 bytes(short) to given port number
 */
void outports(uint16 port, uint16 data) {
    asm volatile("outw %1, %0"
                 :
                 : "dN"(port), "a"(data));
}

/**
 * read 4 bytes(long) from given port number
 */
uint32 inportl(uint16 port) {
    uint32 rv;
    asm volatile("inl %%dx, %%eax"
                 : "=a"(rv)
                 : "dN"(port));
    return rv;
}

/**
 * write given 4 bytes(long) to given port number
 */
void outportl(uint16 port, uint32 data) {
    asm volatile("outl %%eax, %%dx"
                 :
                 : "dN"(port), "a"(data));
}

void shutdown() {
    int brand = cpuid_info(0);
    // QEMU
    if (brand == BRAND_QEMU)
        outports(0x604, 0x2000);
    else
        // VirtualBox
        outports(0x4004, 0x3400);
}

void __cpuid(uint32 type, uint32* eax, uint32* ebx, uint32* ecx, uint32* edx) {
    asm volatile("cpuid"
                 : "=a"(*eax), "=b"(*ebx), "=c"(*ecx), "=d"(*edx)
                 : "0"(type));  // put the type into eax
}

int cpuid_info(int print) {
    uint32 brand[12];
    uint32 eax, ebx, ecx, edx;
    uint32 type;

    memset(brand, 0, sizeof(brand));
    __cpuid(0x80000002, (uint32*)brand + 0x0, (uint32*)brand + 0x1, (uint32*)brand + 0x2, (uint32*)brand + 0x3);
    __cpuid(0x80000003, (uint32*)brand + 0x4, (uint32*)brand + 0x5, (uint32*)brand + 0x6, (uint32*)brand + 0x7);
    __cpuid(0x80000004, (uint32*)brand + 0x8, (uint32*)brand + 0x9, (uint32*)brand + 0xa, (uint32*)brand + 0xb);

    if (print) {
        printf("Brand: %s\n", brand);
        for (type = 0; type < 4; type++) {
            __cpuid(type, &eax, &ebx, &ecx, &edx);
            printf("type:0x%x, eax:0x%x, ebx:0x%x, ecx:0x%x, edx:0x%x\n", type, eax, ebx, ecx, edx);
        }
    }

    if (strstr((const char*)brand, "QEMU") != NULL)
        return BRAND_QEMU;

    return BRAND_VBOX;
}