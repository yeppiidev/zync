# --- SystemZync Makefile ---
#
# > Run `make run` in this folder to run 
#   SystemZync using QEMU (Also compiles
#	the files if they aren't)
#
# > Run `make clean` to remove the build
# 	output
#

# Toolchain
ASM = nasm
CC = gcc
LD = ld
GRUB = /usr/bin/grub-mkrescue

# Sources
SRC = src
ASM_SRC = $(SRC)

# Compiled objects
OBJ = _build
ASM_OBJ = $(OBJ)

# Grub configuration
CONFIG = ./config
OUT = $(OBJ)
INC = ./include
INCLUDE=-I$(INC)

# Other tools
MKDIR = mkdir -p
CP = cp -f

# Add pre-processor defines here
# if needed
DEFINES =

# Assembler flags
ASM_FLAGS = -f elf32
# Compiler flags
CC_FLAGS = $(INCLUDE) $(DEFINES) -m32 -ffreestanding -nostdlib -nodefaultlibs -fno-exceptions -fno-pie -fno-builtin -fno-leading-underscore -Wno-write-strings -fno-use-linker-plugin -Wno-write-strings
# Linker flags
LD_FLAGS = -m elf_i386 -T $(CONFIG)/linker.ld -nostdlib
# QEMU flags
QEMU_FLAGS= -drive file=disk.qcow2,format=raw

# Output BIN binary
TARGET=$(OUT)/SystemZync.bin

# Output ISO image
ISO_DIR=$(OUT)/iso
TARGET_ISO=$(ISO_DIR)/SystemZync.iso

# Assembly source files
ASM_SOURCES=$(ASM_OBJ)/Base/EntryPoint.o \
		    $(ASM_OBJ)/Base/LoadGDT.o\
		    $(ASM_OBJ)/Base/LoadIDT.o \
		    $(ASM_OBJ)/Base/ExceptionHandler.o \
		    $(ASM_OBJ)/Base/IRQHandler.o \
		    $(ASM_OBJ)/Base/BIOS32CallHandler.o \

# C/C++ source files
KERNEL_SOURCES=$(OBJ)/Kernel/IOPorts.o \
		       $(OBJ)/Kernel/GDTBase.o \
		       $(OBJ)/Kernel/IDTBase.o \
		       $(OBJ)/Kernel/ISRHandler.o \
		       $(OBJ)/Kernel/PICService.o \
		       $(OBJ)/Device/Keyboard.o \
		       $(OBJ)/Device/Mouse.o \
		       $(OBJ)/Device/IDE.o \
		       $(OBJ)/Kernel/PMM.o \
		       $(OBJ)/Kernel/KernelHeap.o \
		       $(OBJ)/Kernel/BIOS32_Service.o \
		       $(OBJ)/Kernel.o \
		       $(OBJ)/Lib/String.o \
			   $(OBJ)/Graphics/VGA.o \
		       $(OBJ)/Graphics/VESA.o \
		       $(OBJ)/Graphics/Bitmap.o \
		       $(OBJ)/System/Console.o \
		       $(OBJ)/System/Shell.o \

# Generate build directories
build_directories:
	@$(MKDIR) $(OBJ)
	@$(MKDIR) $(OBJ)/asm
	@$(MKDIR) $(OBJ)/out
	@$(MKDIR) $(OBJ)/iso
	@$(MKDIR) $(OBJ)/Base
	@$(MKDIR) $(OBJ)/Device
	@$(MKDIR) $(OBJ)/Graphics
	@$(MKDIR) $(OBJ)/Lib
	@$(MKDIR) $(OBJ)/System
	@$(MKDIR) $(OBJ)/Kernel

# This is the Makefile entry point
all: build_directories $(ASM_SOURCES) $(KERNEL_SOURCES) link $(TARGET_ISO)
	@grub-file --is-x86-multiboot $(TARGET)
	@rm -f $(TARGET)

# Linker subroutine
link: $(ASM_SOURCES) $(KERNEL_SOURCES)
	@echo "[Linking...]"

	@$(LD) $(LD_FLAGS) -o $(TARGET) $(ASM_SOURCES) $(KERNEL_SOURCES)

# Generate the ISO
$(TARGET_ISO):
	@echo "[Building ISO image...]"

	@$(MKDIR) $(ISO_DIR)/boot/grub
	@$(CP) $(TARGET) $(ISO_DIR)/boot/
	@$(CP) $(CONFIG)/grub.cfg $(ISO_DIR)/boot/grub/
	@$(GRUB) -o $(TARGET_ISO) $(ISO_DIR)

# Compile the assembly source files
$(ASM_OBJ)/%.o: $(ASM_SRC)/%.asm
	@echo "(NASM) [$<]"
	@$(ASM) $(ASM_FLAGS) $< -o $@

# Compile the C/C++ source files
$(OBJ)/%.o: $(SRC)/%.c
	@echo "(CC) [$<]"
	@$(CC) $(CC_FLAGS) -c $< -o $@

# Clean build artifacts
clean:
	@echo -n "Cleaning build artifacts... "
	@rm -rf $(OBJ)
	@echo "Done!"

# Run the ISO using QEMU
run: all
	@echo "[Starting QEMU (i386) with flags '$(QEMU_FLAGS)']"
	@qemu-system-i386 -cdrom $(TARGET_ISO) $(QEMU_FLAGS)